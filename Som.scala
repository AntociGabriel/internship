package playground

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.Done
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.DefaultJsonProtocol._

import scala.io.StdIn

import scala.concurrent.Future

object SprayJsonExample {
  implicit val system = ActorSystem(Behaviors.empty, "SprayExample")
  implicit val executionContext = system.executionContext

  var orders: List[Email] = Nil

  case class Email(id: Long, body: String, sender: String, to: String, cc: String, bcc: String) {
    override def toString: String =
      "[ " + "body: " + body + ", " + "sender: " + sender + ", " + "to: " + to + ", " + "cc: " + cc + ", " +
        "bcc: " + bcc + " ]"

    override def equals(email: Any): Boolean = {
      email match {
        case e: Email => e.body.equals(this.body) && e.sender.equals(this.sender) && e.to.equals(this.to)
        case _ => false
      }
    }
  }
  case class Emails(items: List[Email])

  implicit val itemFormat1 = jsonFormat6(Email)
  implicit val orderFormat1 = jsonFormat1(Emails)

  def fetchItem(itemId: Long): Future[Option[Email]] = Future {
    orders.find(o => o.id == itemId)
  }
//  implicit val order = 2L
//  fetchItem(1)

  def notEmpty: Future[Boolean] = Future {
    orders.nonEmpty
  }
  def remove(email: Email): Future[List[Email]] = Future {
    orders = orders diff List(email)
    orders
  }
  def saveOrder(order: Emails): Future[Done] = {
    orders = orders ++ order.items
//    orders = order match {
//      case Emails(items) => items ++ orders
//      case _ => orders
//    }
    Future { Done }
  }

  def main(args: Array[String]): Unit = {
    val route: Route =
      concat(
        delete {
          pathPrefix("delete" / LongNumber) { id =>
            val maybeItem: Future[Option[Email]] = fetchItem(id)
            onSuccess(maybeItem) {
              case Some(item) => complete(remove(item))
              case None       => complete(StatusCodes.NotFound)
            }
          }
        },
        get {
          pathPrefix("getAll") {
            val maybeItem: Future[Boolean] = notEmpty
            onSuccess(maybeItem) {
              case true => complete(orders)
              case false => complete(StatusCodes.NotFound)
            }
          }
        },
        post {
          path("createEmail") {
            entity(as[Emails]) { order =>
              val saved: Future[Done] = saveOrder(order)
              onSuccess(saved) { _ =>
                complete("order created")
              }
            }
          }
        }
      )

    val bindingFuture = Http().newServerAt("localhost", 8080).bind(route)
    println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }
}
